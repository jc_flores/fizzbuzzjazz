﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Models
{
    public class Request
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public int FizzCount { get; set; }
        public int BuzzCount { get; set; }
        public int JassCount { get; set; }

        public string Response { get; set; }
    }
}
