﻿using DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using webAPI_FizzBuzzJazz.Controllers;
using Xunit;

namespace FizzBuzzJazz_Test
{

   public class FizzBuzzJazz_test
    {

        [Fact]
        public async Task Get_ReturnsValidResponse()
        {
            //Arrange 
            var options = new DbContextOptionsBuilder<APIContext>()
            .UseInMemoryDatabase(databaseName: "NetCoreAPI")
            .Options;

            //Act
            var _dbContext = new APIContext(options);
            var controller = new FizzBuzzJazzController(_dbContext);
            var response = await controller.GetFizzBuzzJazz(3);

            //Assert  
            Xunit.Assert.IsType<OkObjectResult>(response);
        }

        [Fact]
        public async Task Get_ReturnsBadRequest_GivenCeroInput()
        {

            var options = new DbContextOptionsBuilder<APIContext>()
            .UseInMemoryDatabase(databaseName: "NetCoreAPI")
            .Options;
            var _dbContext = new APIContext(options);
            var controller = new FizzBuzzJazzController(_dbContext);


            var response = await controller.GetFizzBuzzJazz(0);
            // Assert
            Assert.IsType<BadRequestResult>(response);
        }

        [Fact]
        public async Task Get_ReturnsValidResponse_Fizz()
        {
            //Arrange 
            var options = new DbContextOptionsBuilder<APIContext>()
            .UseInMemoryDatabase(databaseName: "NetCoreAPI")
            .Options;

            //Act
            var _dbContext = new APIContext(options);
            var controller = new FizzBuzzJazzController(_dbContext);
            var response = await controller.GetFizzBuzzJazz(3);

            //Assert  
            Assert.NotNull(response);          

            List<string> value = (List<string>)((ObjectResult)response).Value;
            Assert.True(value.Contains("Fizz"));

        }

    }
}
