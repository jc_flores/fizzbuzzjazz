
import { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';


function App() {

  const urlservice = "https://localhost:44352/FizzBuzzJazz?value=";
  const [data, setData] = useState([]);
  const [numvalue, setinfowhater] = useState();


  const getData = async () => {

    console.log(numvalue);
    let url = `${urlservice}${numvalue.numvalue}`;
    await axios.get(url)
      .then(response => {
        setData(response.data)
      })
      .catch(error => {
        console.log(error);
      })
  }


  const handleChanges = e => {
    const { name, value } = e.target;
    setinfowhater({
      [name]: value
    })
  }


  return (

    <div className="App mt-3">
      <h3>Fizz-Buzz-Jazz</h3>
      <div className="form-group mt-3">
        <div className="container">
          <div className="row justify-content-center border-bottom pb-4">
            <div className="col-sm-2">
              <label>Number:</label>
            </div>
            <div className="col-sm-1">
              <input type="text" className="form-control" name="numvalue" onChange={handleChanges}></input>
            </div>
            <div className="col-sm-2">
              <button className="btn btn-primary" onClick={() => getData()}>Get response</button>
            </div>
          </div>
          <div className="row justify-content-center border-bottom pb-4">
            <div className="col-2 ">
              <label className="text-primary pl-3">Fizz-</label><label className="text-success pl-3">Buzz-</label><label className="text-danger pl-3">Jazz</label>
            </div>
          </div>
        </div>
      </div>
      <div className="row mt-3 justify-content-center">
        <div className="col-2 ">
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>Response</th>
              </tr>
            </thead>
            <tbody>
              {data.map(item => (
                <tr>
                  <td className={item == "Fizz" ? "text-primary" : item == "Buzz" ? "text-success" : item == "Jazz" ? "text-danger" : "text-dark"}>
                    {item}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}


export default App;
