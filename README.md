# Fizz-Buzz-Jazz

- .Net core Project
- Client react app.
- SQL DB



## Getting started

- Execute `npm install` in Client\multiplenumber folder
- Verify Default connection string for appsetting.
- Execute a `database-update` command for DataAccess project to  apply  EF migration to local DB.

## Run locally
- In Client\multiplenumber folder tun `npm start`. Client react App use http://localhost:3000/
- Run .Net core project, API use https://localhost:44352

