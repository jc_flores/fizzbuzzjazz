﻿using DataAccess;
using DataAccess.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace webAPI_FizzBuzzJazz.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class FizzBuzzJazzController : ControllerBase
    {

        private readonly APIContext _appContext;
        private static string Fizz = "Fizz";
        private static string Buzz = "Buzz";
        private static string Jazz = "Jazz";


        public FizzBuzzJazzController(APIContext appContext)
        {
            this._appContext = appContext;
        }

        [HttpGet]
        public async Task<IActionResult> GetFizzBuzzJazz([FromQuery(Name = "value")] int value)
        {
            if (value <= 0)
            {
                return BadRequest();
            }
            int index = 1;
            List<string> result = new List<string>();
            var request = new Request();

            StringBuilder current = new StringBuilder();

            while (index <= value)
            {
                current = new StringBuilder();
                double x3 = (index % 3);
                double x5 = (index % 5);
                double x7 = (index % 7);

                if (x3 == 0)
                {
                    current.Append(Fizz);
                    request.FizzCount++;
                }
                if (x5 == 0)
                {
                    current.Append(Buzz);
                    request.BuzzCount++;

                }
                if (x7 == 0)
                {
                    current.Append(Jazz);
                    request.JassCount++;
                }

                else if( string.IsNullOrEmpty(current.ToString()))
                {
                    current.Append(index.ToString());
                }


                result.Add(current.ToString());
                index++;
            }

            request.Response = string.Join(",",result);

            this._appContext.Request.Add(request);
           await this._appContext.SaveChangesAsync();

            return Ok(result);
        }

    }
}
